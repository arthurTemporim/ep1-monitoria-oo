#ifndef IMAGEM_PPM_HPP
#define IMAGEM_PPM_HPP

#include "imagem.hpp"

//Associacao.
#include "pixel.hpp"

using namespace std;

class Imagem_ppm : public Imagem {
	private:

    //Este atributo e um vetor do tipo Pixel.
    Pixel *pix;
    
	public:
		Imagem_ppm();

    //Construtor com vetor de objetos Pixel.
		Imagem_ppm(string numeoro_magico,
        		   string comentario,
        		   unsigned int largura,
        	 	   unsigned int altura,
        		   unsigned int cor_maxima,
               Pixel *pix);

		//Construtor sem vetor de objetos Pixel.
		Imagem_ppm(string numeoro_magico,
        		   string comentario,
        		   unsigned int largura,
        	 	   unsigned int altura,
        		   unsigned int cor_maxima);

		//Destrutor
    ~Imagem_ppm();
    
    //Metodos
    void decifraPPM(string nome_nova_imagem, string pixels);
    void inserePixObjeto(string pixels);
    void aplicaFiltro(string nome_nova_imagem, string pixels);
    Pixel transformaImagem(char cor, Pixel unico_pix);
    void inserePixArquivo(Pixel *novo_pix, ofstream *nova_imagem);
    
    //Acessadores.
    Pixel *getPixel();
    void setPixel(Pixel *pix);
};
#endif
