#ifndef IMAGEM_HPP
#define IMAGEM_HPP

//Os includes aqui feitos vao para todas as classes que herdam.
#include <string>
#include <fstream>
#include <iostream>
#include <cstdlib>

class Imagem {

    //Atributos.
    private:

        std::string numero_magico;
        std::string comentario;
        unsigned int largura;
        unsigned int altura;
        unsigned int cor_maxima;

    //Métodos.
    public:

        //Construtor vazio.  
        Imagem();

        //Construtor de cabeçalho
        Imagem(std::string numeoro_magico,
        std::string comentario,
        unsigned int largura,
        unsigned int algura,
        unsigned int cor_maxima);

        //virtual ~Imagem();
        //Acessadores
        std::string getNumero_magico();
        void setNumero_magico(std::string numero_magico);
        
        std::string getComentario();
        void setComentario(std::string);

        unsigned int getLargura();
        void setLargura(unsigned int largura);

        unsigned int getAltura();
        void setAltura(unsigned int altura);

        unsigned int getCor_maxima();
        void setCor_maxima(unsigned int cor_maxima);

        virtual std::string CarregaImagem(std::string nome_da_imagem);
};

#endif
