#ifndef IMAGEM_PGM_HPP
#define IMAGEM_PGM_HPP

#include "imagem.hpp"

//Para não precisar colocar "std::" antes de cada string nessa classe.
using namespace std;

class Imagem_pgm : public Imagem {

  //Novo atributo para este tipo de imagem.
  private:

    //String com todos os pixels da imagem (contém a imagem em si).
    string pixel;

  public:

    Imagem_pgm();
    //Controi um objeto sem a string de pixels.
    Imagem_pgm(string numeoro_magico,
        string comentario,
        unsigned int largura,
        unsigned int algura,
        unsigned int cor_maxima);

    //Constroi um objeto com a string de pixels.
    Imagem_pgm(string numeoro_magico,
        string comentario,
        unsigned int largura,
        unsigned int algura,
        unsigned int cor_maxima,
        string pixel);

    //Acessadores do novo atributo.
    string getPixel();
    void setPixel(string pixel);

    //Métodos implementados na cpp.
    void DecifraMensagem(string pixels_imagem);
    unsigned int DescobreComecoMensagem();
    unsigned char DescobreLetra(string pixels_imagem, int numero_pixel);

};

#endif
