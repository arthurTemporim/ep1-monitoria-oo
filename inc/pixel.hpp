#ifndef PIXEL_HPP
#define PIXEL_HPP

class Pixel {

	private:

		char r;
		char g;
		char b;

	public:

		Pixel();
		Pixel(char r, char g, char b);

		char getR();
		void setR(char r);

		char getG();
		void setG(char g);

		char getB();
		void setB(char b);
};

#endif
