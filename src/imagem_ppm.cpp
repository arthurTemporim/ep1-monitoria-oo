#include "imagem_ppm.hpp"

using namespace std;

//Construtores.
Imagem_ppm::Imagem_ppm() {
  setNumero_magico("NumeroMagico");
  setComentario("Comentario");
  setLargura(0);
  setAltura(0);
  setCor_maxima(0);
}

//Construtor com vetor de Pixel.
Imagem_ppm::Imagem_ppm(string numero_magico,
    string comentario,
    unsigned int largura,
    unsigned int altura,
    unsigned int cor_maxima,
    Pixel *pix) {
  setNumero_magico(numero_magico);
  setComentario(comentario);
  setLargura(largura);
  setAltura(altura);
  setCor_maxima(cor_maxima);
  setPixel(pix); 
}

//Construtor sem vetor de Pixel.
Imagem_ppm::Imagem_ppm(string numero_magico,
    string comentario,
    unsigned int largura,
    unsigned int altura,
    unsigned int cor_maxima) {
  setNumero_magico(numero_magico);
  setComentario(comentario);
  setLargura(largura);
  setAltura(altura);
  setCor_maxima(cor_maxima);
}


//Metodos.  
void Imagem_ppm::inserePixObjeto(string pixels)  {

  unsigned int tamanho_pixels = pixels.size();

  Pixel *pix = (Pixel*) malloc(tamanho_pixels);

  cout << pix->getR() << endl;

  //Iterador.
  unsigned int i = 0;
  unsigned int j = 0;

  //Insere os pixels da string no vetor de objetos pix;
  while(i < tamanho_pixels) {
    pix[j].setR(pixels[i]);
    i++;
    pix[j].setG(pixels[i]);
    i++;
    pix[j].setB(pixels[i]);
    i++;
    j++;
  }
  setPixel(pix);
}

//Decifra mensagem.
void Imagem_ppm::decifraPPM(string nome_nova_imagem, string pixels) {
  aplicaFiltro(nome_nova_imagem, pixels);
}

//Descobre o filtro a ser aplicado.
void Imagem_ppm::aplicaFiltro(string nome_nova_imagem, string pixels) {

  //Insere os pixels da string no atributo da imagem.
  inserePixObjeto(pixels);

  nome_nova_imagem += ".ppm";
  //Cria nova imagem.
  ofstream nova_imagem(nome_nova_imagem.c_str());

  //Insere cabeçalho na nova imagem.
  nova_imagem << getNumero_magico() << endl;
  nova_imagem << getComentario() << endl;
  nova_imagem << getLargura() << endl;
  nova_imagem << getAltura() << endl;
  nova_imagem << getCor_maxima() << endl;


  // Pega o comentario do objeto.
  string comentario = getComentario();

  //Recebe o tamanho do vetor de Pixels.
  //unsigned int tamanho_pixs = tamanho_pix;
  unsigned int tamanho_pix = getAltura() * getLargura();

  //Pega o vetor de pix.
  Pixel *vetor_pix = getPixel();

  //Vetor de pix transformado.
  Pixel *novo_vetor_pix = (Pixel*) malloc(tamanho_pix * sizeof(Pixel));

  //Iterador.
  unsigned int i = 0;

  // A segunda letra do comentario e R, G ou B entao da pra descobrir o tipo de filtro por ela.
  char filtro = comentario[1];
  if(filtro != 'R' && filtro != 'G' && filtro != 'B') {
      cout << "Houve um problema na aplicaçao do filtro." << endl;
      cout << "O programa sera encerrado." << endl;
      exit(EXIT_FAILURE);
  } else {
    while(i < tamanho_pix) {
      novo_vetor_pix[i] = transformaImagem(filtro, vetor_pix[i]);
      i++;
    }
  inserePixArquivo(novo_vetor_pix, &nova_imagem);
}
}

//Metodo que transforma os pixels da imagem ppm.
Pixel Imagem_ppm::transformaImagem(char cor, Pixel unico_pix) {
  //Cria novo pix pra retornar.  
  Pixel novo_pix;

  //Se o filtro for R repete o valor, senao coloca 0.
  novo_pix.setR(cor == 'R' ? unico_pix.getR() : 0);
  novo_pix.setG(cor == 'G' ? unico_pix.getG() : 0);
  novo_pix.setB(cor == 'B' ? unico_pix.getB() : 0);

  return novo_pix;  
}

//Insere o novo pix em uma nova imagem.
void Imagem_ppm::inserePixArquivo(Pixel *novo_pix, ofstream *nova_imagem) {


  unsigned int i = 0;

  unsigned int tamanho_pix = getLargura() * getAltura();

  for(i = 0; i < tamanho_pix; i++) {  
    nova_imagem -> put(novo_pix[i].getR());
    nova_imagem -> put(novo_pix[i].getG());
    nova_imagem -> put(novo_pix[i].getB());
  }
  nova_imagem->close();
}

//Acessadores.
Pixel *Imagem_ppm::getPixel() {
  return pix;
}

void Imagem_ppm::setPixel(Pixel *pix) {
  this -> pix  = pix;
}
