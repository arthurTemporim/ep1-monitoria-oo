#include "pixel.hpp"

using namespace std;

//Construtores.
Pixel::Pixel(){
	setR('r');
	setG('g');
	setB('b');
}

Pixel::Pixel(char r, char g, char b) {
	setR(r);
	setG(g);
	setB(b);
}

//Acessadores.
char Pixel::getR() {
	return r;
}
void Pixel::setR(char r) {
	this -> r = r;
}

char Pixel::getG() {
	return g;
}
void Pixel::setG(char g) {
	this -> g = g;
}

char Pixel::getB() {
	return b;
}
void Pixel::setB(char b) {
	this -> b = b;
}
