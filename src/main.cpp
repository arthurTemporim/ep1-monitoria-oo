#include "imagem.hpp"
#include "imagem_pgm.hpp"
#include "imagem_ppm.hpp"

using namespace std;

//Sintese de métodos.
string RecebeEntrada(string nome_da_imagem);
unsigned int DescobreTipoImagem(Imagem *imagem_inicial);
void DecifraImagem(Imagem *imagem_inicial, string pixels);





//MAIN
int main() {

  //Instancia de um objeto Imagem default.
  Imagem *imagem_inicial = new Imagem();
  string nome_da_imagem = "";  

  nome_da_imagem = RecebeEntrada(nome_da_imagem);

  string pixels = "";

  //Pega o cabeçalho da imagem e insere no objeto. Retorna pixels.
  pixels = imagem_inicial -> CarregaImagem(nome_da_imagem);
  DecifraImagem(imagem_inicial, pixels);

  return 0;
}





void DecifraImagem(Imagem *imagem_inicial, string pixels) {
  //cout << pixels << endl;
  unsigned int tipo_imagem = 0;
  tipo_imagem = DescobreTipoImagem(imagem_inicial);
  if (tipo_imagem == 1) {
    //Decifra PGM.
    //Instancia de um objeto Imagem_pgm.
    Imagem_pgm *imagem_pgm = new Imagem_pgm(imagem_inicial->getNumero_magico(),
        imagem_inicial->getComentario(),
        imagem_inicial->getLargura(),
        imagem_inicial->getAltura(),
        imagem_inicial->getCor_maxima(),
        pixels);
    imagem_pgm -> DecifraMensagem(imagem_pgm -> getPixel());

    cout << "=============================================================" << endl;
    cout << "Fim do programa." << endl;
    cout << "=============================================================" << endl;

  } else if (tipo_imagem == 2) {
    //Decifra PPM.
    //Instancia de um objeto Imagem_ppm.    
    Imagem_ppm *imagem_ppm = new Imagem_ppm(imagem_inicial->getNumero_magico(),
        imagem_inicial->getComentario(),
        imagem_inicial->getLargura(),
        imagem_inicial->getAltura(),
        imagem_inicial->getCor_maxima());
    string pixels_ppm = "";

    //Metodo que pega os pixels do arquivo ppm.
    pixels_ppm = pixels;
    //Metodo de solucao.

    
    string entrada_nome_nova_imagem = "";
    cout << "=============================================================" << endl;
    cout << "Digite o nome da imagem de saida (sem caminho e sem formato)." << endl;
    cout << "=============================================================" << endl;
    cin >> entrada_nome_nova_imagem;

    string nome_nova_imagem = "doc/saida/";
    nome_nova_imagem += entrada_nome_nova_imagem;
    imagem_ppm -> decifraPPM(nome_nova_imagem, pixels_ppm);

    cout << "=============================================================" << endl;
    cout << "A mensagem foi salva em doc/saida/" << endl;
    cout << "Fim do programa." << endl;
    cout << "=============================================================" << endl;
  } else {
    //Erro inexperado.
    cout << "Nao foi possivel decifrar a mensagem" << endl;
    cout << "Programa sera encerrado :(" << endl;
    exit(EXIT_FAILURE);
  }
}

//Descobre o tipo de imagem e imprime na tela.
unsigned int DescobreTipoImagem(Imagem *imagem_inicial) {

  //Guarda o tipo da imagem de acordo com o numero magico.
  unsigned int tipo_imagem = 0;
  cout << endl << "======================" << endl;

  //Se P5 é PGM, se P6 é PPM.
  if (imagem_inicial -> getNumero_magico() == "P5") {
    cout << "A IMAGEM É DO TIPO PGM";
    tipo_imagem = 1;
  } else if (imagem_inicial -> getNumero_magico() == "P6") {
    cout << "A IMAGEM É DO TIPO PPM";
    tipo_imagem = 2;
  } else {
    //Caso inesperado.
    cout << "Numero mágico da imagem é inválido." << endl;
    cout << "O programa será encerrado :(" << endl;
    exit (EXIT_FAILURE);
  }
  cout << endl << "======================" << endl;

  //Retorna 1 se a imagem for PGM e 2 se a imagem for PPM.
  return tipo_imagem;
}

string RecebeEntrada(string nome_da_imagem) {

  cout << "==================================================="<< endl;
  cout << "Digite o nome da imagem para descobrir um segrendo." << endl;
  cout << "==================================================="<< endl;

  cin >> nome_da_imagem;

  return nome_da_imagem;
}
