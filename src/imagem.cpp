#include "imagem.hpp"

using namespace std;

//Métodos de mais relevancia no comeco do arquivo.
//Construtor Vazio
Imagem::Imagem() {

//Atribui valores default.
  setNumero_magico("NumeroMagico");
  setComentario("ConstrutorNulo");
  setLargura(0);
  setAltura(0);
  setCor_maxima(255);
}

//Construtor
Imagem::Imagem(string numero_magico,
               string comentario,
               unsigned int largura,
               unsigned int altura,
               unsigned int cor_maxima) {

//Atribui valores recebidos na instancia do objeto.
  setNumero_magico(numero_magico);
  setComentario(comentario);
  setLargura(largura);
  setAltura(altura);
  setCor_maxima(cor_maxima);
}

//Carrega o cabeçalho da imagem.
string Imagem::CarregaImagem(string nome_da_imagem) {
  
  //Com o nome inserido carrega a imagem.
  ifstream arquivo_imagem(nome_da_imagem.c_str());
  
  //Verifica se o nome_da_imagem é valido.
  //Se o nome for válido pega o cabeçalho, se não sai do programa.
  if (arquivo_imagem.is_open()) {
    //Fluxo principal.
    //Variavel que guarda os campos da imagem.  
    string conteudo_imagem = "";

    //Mensagem no terminal para ver cabeçalho da imagem.
    cout << endl << endl;
    cout << "============================" << endl;
    cout << "IMPRIME CABEÇALHO DA IMAGEM." << endl;
    cout << "============================" << endl;

    //Pega o cabeçalho da imagem usando metodos acessadores.
    getline(arquivo_imagem, conteudo_imagem);
    setNumero_magico(conteudo_imagem);
    cout << "Número mágico: " << getNumero_magico() << endl;

    getline(arquivo_imagem, conteudo_imagem);
    setComentario(conteudo_imagem);
    cout << "Comentário: " << getComentario() << endl;

    unsigned int int_imagem;
    arquivo_imagem >> int_imagem;
    setLargura(int_imagem);
    cout << "Largura: " << getLargura() << endl;

    arquivo_imagem >> int_imagem;
    setAltura(int_imagem);
    cout << "Altura: " << getAltura() << endl;

    arquivo_imagem >> int_imagem;
    setCor_maxima(int_imagem);
    cout << "Nível máximo de cor: " << getCor_maxima() << endl;

    //Pega uma linha vazia.
    getline(arquivo_imagem, conteudo_imagem);
    
    //Pega os pixels da imagem.
    if(getNumero_magico() == "P5") {

      getline(arquivo_imagem, conteudo_imagem);

    } else if (getNumero_magico() == "P6") {
      char letra = ' ';
      while(!arquivo_imagem.eof()) {             
         arquivo_imagem.get(letra);               
         conteudo_imagem += letra;                         
      }  

    } else {
      cout << "Erro ao retornar pixels do arquivo de entrada!" << endl; 
      cout << "O programa será encerrado :(" << endl;
      exit(EXIT_FAILURE);
    }

    //Fecha arquivo
    arquivo_imagem.close();

    //Retorna os pixels da imagem dentro de uma string.
    return conteudo_imagem;

  } else {
    //Fluxo alternativo.
    cout << endl << endl;
    cout << "Desculpe, caminho invalido :(" << endl;
    cout << "Programa vai fechar!" << endl;
    cout << endl << endl;

    //Força saida do programa.
    exit(EXIT_FAILURE);
  }

}


//Encapsulamento
string Imagem::getNumero_magico() {
  return numero_magico;
}  
void Imagem::setNumero_magico(string numero_magico) {
  this -> numero_magico = numero_magico;
}

string Imagem::getComentario() {
  return comentario;
}
void Imagem::setComentario(string comentario) {
  this -> comentario = comentario;
}

unsigned int Imagem::getLargura() {
  return largura;
}
void Imagem::setLargura(unsigned int largura) {
  this -> largura = largura;
}

unsigned int Imagem::getAltura() {
  return altura;
}
void Imagem::setAltura(unsigned int altura) {
  this -> altura = altura;
}

unsigned int Imagem::getCor_maxima() {
  return cor_maxima;
}
void Imagem::setCor_maxima(unsigned int cor_maxima) {
  this -> cor_maxima = cor_maxima;
}
