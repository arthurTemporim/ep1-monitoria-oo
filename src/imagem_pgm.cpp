#include "imagem_pgm.hpp"

using namespace std;

//Construtores.
Imagem_pgm::Imagem_pgm() {
  setNumero_magico("NumeroMagico");
  setComentario("Comentario");
  setLargura(0);
  setAltura(0);
  setCor_maxima(0);
  setPixel(" ");
}

Imagem_pgm::Imagem_pgm(string numero_magico,
    string comentario,
    unsigned int largura,
    unsigned int altura,
    unsigned int cor_maxima) {
  setNumero_magico(numero_magico);
  setComentario(comentario);
  setLargura(largura);
  setAltura(altura);
  setCor_maxima(cor_maxima);
}

Imagem_pgm::Imagem_pgm(string numero_magico,
    string comentario,
    unsigned int largura,
    unsigned int altura,
    unsigned int cor_maxima,
    string pixel) {
  setNumero_magico(numero_magico);
  setComentario(comentario);
  setLargura(largura);
  setAltura(altura);
  setCor_maxima(cor_maxima);
  setPixel(pixel);
}

//Acessadores.
string Imagem_pgm::getPixel() {
  return pixel;
}
void Imagem_pgm::setPixel(string pixel) {
  this -> pixel = pixel;
}

//Métodos que decifram a mensagem.
void Imagem_pgm::DecifraMensagem(string pixels_imagem) {
  unsigned int comeco = 0;	
  comeco = DescobreComecoMensagem();
  cout << "A mensagem começa no Pixel: " << comeco << "." << endl;
  cout << endl;


  unsigned char letra = ' ';
  int i = comeco;

  //Se o começo da mensagem, for o começo da lena.pgm (50000), acrescenta 8.
  if(comeco == 50000) {
    i +=8;
  } else {
    //Tratamento para a mensagem na lena
    //Nada a fazer
  }

  cout << "=============" << endl;
  cout << "A mensagem é:" << endl;
  cout << "=============" << endl;

  while(letra != '#') { 
    letra = DescobreLetra(pixels_imagem, i);
    cout << letra;
    i += 8;
  }
  cout << endl;

}


unsigned int Imagem_pgm::DescobreComecoMensagem() {
  //Pega o comentario no atributo e joga em uma variavel.
  string conteudo_comentario = " ";
  conteudo_comentario = getComentario();

  //Verifica valor da variável.  
  //cout << "Comentario: " << conteudo_comentario << endl;

  //Assume que o numero de comeco da mensagem tem apenas 10 digitos.
  char inicio_mensagem[11] = {' '};  

  //Percorre comentario
  int i = 0;
  //Incrementa o inicio_mensagem.
  int j = 0;

  //Descobre o tamanho do comentario.
  int tamanho_comentario = 0;
  tamanho_comentario = conteudo_comentario.size();

  //Possui uma falha. Pega todos os números do comentário.
  while(i <= tamanho_comentario) {

    //Pega char no intervalo dos números da tabela ASCII.
    if (conteudo_comentario[i] >= 48 && conteudo_comentario[i] <= 57) {

      //Verifica valor do comentário.
      //cout << i << " " << inicio_mensagem[i] << endl;
      //cout << i << " " << conteudo_comentario[i] << endl;

      inicio_mensagem[j++] = conteudo_comentario[i];
    } else  {
      //Fluxo alternativo.
      //Nothing to do.
    }
    i++;
  }

  //Transforma a string em inteiro.
  unsigned int inicio_mensagem_int = 0;
  inicio_mensagem_int = atoi(inicio_mensagem);

  //Verifica o valor do começo da mensagem.
  //cout << "Inicio: " << inicio_mensagem_int << endl;

  //Retorna o valor do começo do comentario.
  return inicio_mensagem_int;
}

//Recebe uma string com toda imagem e posição inicial do pixel a ser descoberto.
unsigned char Imagem_pgm::DescobreLetra(string pixels_imagem, int numero_pixel) {
  unsigned char letra = 0;
  unsigned char pix_da_imagem = 0;

  //Percorre 8 pixels para retornar uma letra da mensagem.
  for(int i = 0;i < 8; i++) {
    pix_da_imagem = (0xff & pixels_imagem[numero_pixel]);
    letra = letra << 1;

    letra = (letra & 0xFE) | (pix_da_imagem & 0x01);

    numero_pixel++;

    //Verifica o valor do char em binário.
    //imprimeValorBit(letra);
  }

  return letra;
}

//Método usado apenas para verificar o valor dos bits.
void imprimeValorBit(char letra) {
  unsigned int bit;
  bit = letra & 0x01;
  cout << bit << " ";
}
