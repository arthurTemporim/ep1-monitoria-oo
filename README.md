# README

## Introdução

  O objetivo deste projeto é um programa capaz de ler arquivos de imagens nos formatos PGM e PPM, além disso, extrair informações embutidas nestas imagens através da técnica de esteganografia.

## O que o software faz

  * Abre imagens no formato **PPM** (coloridas) e **PGM** (preto e branco).
  * Descobre mensagem escondida em formato texto nas imagens PGM.
  * Descobre mensagem escondida em imagens coloridas por meio de aplicação de filtros.
  * Imprime na tela a mensagem descoberta (PGM).
  * Cria nova imagem solucionanda na pasta doc/saida/ (PPM).

## Instruções para executar o MAKEFILE:

  * Digite `make` para compilar.
  * Digite `make run ` para executar.
  * Digite o caminho, nome e formato da imagem a ser inserida.
  * `doc/imagem1.pgm` ou `doc/mensagem1.ppm`.
  * Caso a imagem seja ppm digite o nome da saida sem `doc/` e sem `.ppm`.
  
## Estrutura de pastas:
  * **bin/** Contém os arquivos binários após compilado.
  * **doc/** Contém documentos relacionados ao projeto, e as imagens.
  * **inc/** Contém os arquivos `.hpp`.
  * **obj/** Contém os arquivos `.o` após compilado.
  * **src/** Contém os arquivos  `.cpp`.